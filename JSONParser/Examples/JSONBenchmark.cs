﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace TogglePlay.Examples
{

	/// <summary>
	/// Parses a text asset once per frame and displays
	/// how long it taken to parse
	/// </summary>
	public class JSONBenchmark : MonoBehaviour
	{
		//----------------------------------------------------------------------------------
		// Inspector Variables
		//----------------------------------------------------------------------------------

		[Header("UI Benchmark labels")]

		[Tooltip("Label to display the average ticks it takes to parse the file")]
		[SerializeField] private Text averageLabel = null;

		[Tooltip("Label to display the shortest recorded ticks taken to parse the file")]
		[SerializeField] private Text shortestLabel = null;

		[Tooltip("Label to display the longest recorded ticks taken to parse the file")]
		[SerializeField] private Text longestLabel = null;


		[Header("To Benchmark json")]

		[Tooltip("The json text file to parse")]
		[SerializeField] private TextAsset jsonText = null;

		//----------------------------------------------------------------------------------
		// Member Variables
		//----------------------------------------------------------------------------------

		/// <summary>
		/// The shortest ticks taken to parse the json file
		/// </summary>
		private long shortest = long.MaxValue;

		/// <summary>
		/// The longest ticks taken to parse the json file
		/// </summary>
		private long longest = 0;

		/// <summary>
		/// An array of the latest 256 tick counts, to use to calculate and average
		/// </summary>
		private long[] averageArray = new long[256];

		/// <summary>
		/// How many entries have been recorded into the average array
		/// </summary>
		private int averageLength = 0;

		/// <summary>
		/// Which position in the array to write to next
		/// </summary>
		private int currentPos = 0;

		//----------------------------------------------------------------------------------
		// Pasing loop
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Loop each frame adding a new tick count
		/// </summary>
		private IEnumerator Start()
		{
			// use a stop watch to time how many ticks it takes to parse the file
			System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

			// parse a json object each frame, updating the time it takes each time
			while( true )
			{
				watch.Reset();
				watch.Start();

				// parse
				TogglePlay.JSON.Parse( jsonText.text );

				watch.Stop();

				// change timers
				long c = watch.ElapsedTicks;

				if( c < shortest )
					shortest = c;
				if( c > longest )
					longest = c;

				averageArray[currentPos] = c;
				averageLength = averageLength >= 256 ? 256 : averageLength + 1;
				currentPos = currentPos >= 255 ? 0 : currentPos + 1;

				c = 0;
				for( int i=0; i<averageLength; ++i )
				{
					c += averageArray[i];
				}
				c /= averageLength;

				averageLabel.text = c.ToString();
				shortestLabel.text = shortest.ToString();
				longestLabel.text = longest.ToString();

				yield return null;
			}
		}
	}

}

