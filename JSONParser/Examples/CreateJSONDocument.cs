﻿using UnityEngine;
using System.Collections;

namespace TogglePlay
{

	public class CreateJSONDocument : MonoBehaviour
	{
		void Start()
		{
			JSON doc = new JSON();
			// TODO remove this requirement
			doc.CreateNew();

			doc.AddData( new JSON.Data( "version", 1 ) );

			JSON.Data enemies = new JSON.Data( "enemies", null );
			enemies.isObject = true;
			doc.AddData( enemies );

			//----------------------------------------------------------------------------------
			// Add children to enemies, by first creating the Data

			JSON.Data levels = new JSON.Data( "firstLevels", null );
			levels.isArray = true;
			levels.AddChild( 8 );
			levels.AddChild( 3 );
			levels.AddChild( 6 );
			enemies.AddChild( levels );

			JSON.Data orge = new JSON.Data( "orge", null );
			orge.isObject = true;
			orge.AddChild( "name", "Arnold" );
			orge.AddChild( "id", 1 );
			enemies.AddChild( orge );

			//----------------------------------------------------------------------------------
			// Add children to enemies by adding directly to the Data

			enemies.AddEmptyArray( "secondLevels" );
			enemies["secondLevels"].AddChild( 9 );
			enemies["secondLevels"].AddChild( 4 );
			enemies["secondLevels"].AddChild( 7 );

			enemies.AddEmptyObject( "rat" );
			enemies["rat"]["name"].SetValue( "Frank" );
			enemies["rat"].AddChild( "id", 2 );

			//----------------------------------------------------------------------------------
			// Log data to ensure it is there

			Debug.Log( "rat enemy name is : " + doc["enemies"]["rat"]["name"].AsString );
			Debug.Log( "orge enemy name is : " + doc["enemies"]["orge"]["name"].AsString );
			Debug.Log( "First level is : " + doc["enemies"]["firstLevels"][0].AsInt );
			Debug.Log( "Second level is : " + doc["enemies"]["secondLevels"][0].AsInt );

			JSON.Data o = null;
			if( doc.TryGetElement( "version", out o ) )
				Debug.Log( "Version is : " + o.AsInt );
			else
				Debug.Log( "ERROR" );

			//----------------------------------------------------------------------------------
			// Write document to a file


			Debug.Log( "ToJSON = " + doc.ToJSON() );
			Debug.Log( "ToJSON = " + doc["enemies"]["orge"]["id"].ToJSON() );
		}
	}

}