﻿#define REFLECTION_PARSING

using UnityEngine;
using System;
using System.Collections;

namespace TogglePlay.Examples
{

	/// <summary>
	/// Example script showing different ways of reading data from json text
	/// </summary>
	public class JSONParsingExample : MonoBehaviour
	{
		//----------------------------------------------------------------------------------
		// Member Declarations
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Enemy data class
		/// </summary>
		[System.Serializable]
		public class Enemy
		{
			public string name = null;
			public int id = 0;
			public int level = 1;
			public int health = 100;
			public int attack = 10;
		}

		/// <summary>
		/// NPC data class
		/// </summary>
		[System.Serializable]
		public class NPC
		{
			public string name = null;
			public int id = 0;

			// keyName used when using reflection parsing to get the data from the "items" element of the json object
			[JSON.KeyName( "stock" ), JSON.KeyName( "items" )]
			public int[] inventory = null;
		}

		//----------------------------------------------------------------------------------
		// Inspector Variables
		//----------------------------------------------------------------------------------

		[Tooltip( "The json text to read containing the data" )]
		[SerializeField] private TextAsset jsonFile = null;

		//----------------------------------------------------------------------------------
		// Member Variables
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Enemy data created from the data read form the json text
		/// </summary>
		Enemy[] enemies = null;

		/// <summary>
		/// NPC data created from the data read form the json text
		/// </summary>
		NPC[] npcs = null;

		//----------------------------------------------------------------------------------
		// Parse the json data on start
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Parse the json text and read any Enemy / NPC data found
		/// </summary>
		void Start()
		{
			//----------------------------------------------------------------------------------
			// Try parsing the json text file

			JSON json = new JSON();
			if( json.ParseString( jsonFile.text ) != JSON.EParseResult.SUCCESS )
			{
				Debug.LogError( "Failed to parse" );
				return;
			}

			//----------------------------------------------------------------------------------
			// Parse the enemy data using the Reflection class to automatically assign values of the same name

			JSON.Data data_enemies = json["enemies"];
			if( data_enemies.Type == JSON.Data.EType.ARRAY )
			{
				enemies = new Enemy[data_enemies.Count];
				for( int i=0; i<enemies.Length; ++i )
					enemies[i] = data_enemies[i].FromObject<Enemy>();
			}


			//----------------------------------------------------------------------------------
			// Parse the NPC data manually

			JSON.Data data_npcs = json["npcs"];
			if( data_npcs.Type == JSON.Data.EType.ARRAY )
			{
				npcs = new NPC[data_npcs.Count];
				for( int i=0; i<npcs.Length; ++i )
				{
					// If using relection parsing, parse AsObject
#if REFLECTION_PARSING
					npcs[i] = data_npcs[i].FromObject<NPC>();
#else
					npcs[i] = new NPC();
					npcs[i].name = data_npcs[i]["name"].AsString;
					npcs[i].id = data_npcs[i]["id"].AsInt;
					
					JSON.Data data_inventory = data_npcs[i]["items"];
					npcs[i].inventory = new int[data_inventory.Count];
					for( int item=0; item<data_inventory.Count; ++item )
					{
						int itemID = 0;
						
						// Try functions can be used if uncertain that an element exists
						if( data_inventory[item].TryGetInt( out itemID ) )
							npcs[i].inventory[item] = itemID;
						else
							Debug.LogError( "Failed to load inventory correctly" );
					}
#endif
				}
			}


			//----------------------------------------------------------------------------------
			// Log the results

			for( int i=0; i<enemies.Length; ++i )
			{
				Debug.Log( "New enemy : " + enemies[i].name );
			}

			for( int i=0; i<npcs.Length; ++i )
			{
				if( npcs[i] == null || npcs[i].inventory == null )
					continue;
				Debug.Log( "NPC \"" + npcs[i].name + "\" has " + npcs[i].inventory.Length + " items in their inventory" );
			}
		}
	}

}
