﻿using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	/// <summary>
	/// Example script showing reading enemy data from json text and instantiating prefabs
	/// based upon the data within the text file.
	/// </summary>
	public class JSONSimpleEnemyExample : MonoBehaviour
	{
		//----------------------------------------------------------------------------------
		// Member Declarations
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Enemy data container
		/// </summary>
		[System.Serializable]
		public class Enemy
		{
			public string name = null;
			public int id = 0;
			public string asset = "";
			public int level = 1;
			public int health = 100;
			public int attack = 10;
		}

		//----------------------------------------------------------------------------------
		// Inspector Variables
		//----------------------------------------------------------------------------------

		[Tooltip( "The json file containing the enemy spawn data" )]
		[SerializeField] private TextAsset jsonFile = null;

		[Tooltip( "Prefabs available, asset name used to identify them" )]
		[SerializeField] private GameObject[] assetPrefabs = null;

		//----------------------------------------------------------------------------------
		// Member Variables
		//----------------------------------------------------------------------------------

		/// <summary>
		/// The enemy data parsed from the json file
		/// </summary>
		Enemy[] enemies = null;

		//----------------------------------------------------------------------------------
		// Enemy Functions
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Finds the enemy data for the enemy with the id specified
		/// </summary>
		/// <returns>The enemy.</returns>
		/// <param name="id">Identifier number of the enemy to look for.</param>
		Enemy FindEnemy( int id )
		{
			for( int i=0; i<enemies.Length; ++i )
			{
				if( enemies[i].id == id )
					return enemies[i];
			}
			return null;
		}

		/// <summary>
		/// Gets a game object prefab with the name provided
		/// </summary>
		/// <returns>The enemy prefab object.</returns>
		/// <param name="assetName">Name of the prefab to search for.</param>
		GameObject FindEnemyPrefab( string assetName )
		{
			for( int i=0; i<assetPrefabs.Length; ++i )
			{
				if( assetPrefabs[i].name == assetName )
					return assetPrefabs[i];
			}
			return null;
		}

		//----------------------------------------------------------------------------------
		// Parse the json data on start
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Spawn the prefabs on start and setup the "enemies" based on the data from the json
		/// </summary>
		void Start()
		{
			JSON json = new JSON();
			if( json.ParseString( jsonFile.text ) != JSON.EParseResult.SUCCESS )
			{
				Debug.LogError( "Failed to parse" );
				return;
			}

			//----------------------------------------------------------------------------------
			// Parse the enemy data using the Reflection class to automatically assign values of the same name

			JSON.Data data_enemies = json["enemies"];
			if( data_enemies.Type == JSON.Data.EType.ARRAY )
			{
				enemies = new Enemy[data_enemies.Count];
				for( int i=0; i<enemies.Length; ++i )
					enemies[i] = data_enemies[i].FromObject<Enemy>();
			}


			//----------------------------------------------------------------------------------
			// Spawn enemies

			JSON.Data data_spawn = json["spawn"];
			if( data_spawn.Type == JSON.Data.EType.ARRAY )
			{
				for( int i=0; i<data_spawn.Count; ++i )
				{
					JSON.Data data_enemy = data_spawn[i];

					Enemy toSpawn = FindEnemy( data_enemy["enemy"].AsInt );
					float size = data_enemy["size"].AsFloat;

					GameObject prefab = FindEnemyPrefab( toSpawn.asset );
					GameObject spawnedEnemy = Instantiate( prefab, data_enemy["position"].AsVector3, Quaternion.identity ) as GameObject;

					spawnedEnemy.transform.localScale = new Vector3( size, size, size );
				}
			}
		}
	}

}
