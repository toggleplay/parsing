﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: This is a JSON parser specfically made with unity in mind with commands such as
 * .AsVector3, .AsColor and logging errors to the unity console.
 * 
 * Tasks:
 * - Test on all platforms.
 * Currently tested on, windows osX iOS and Android
 * Needed on Windows Phone, webGL and any other to be supported
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay
{
	public partial class JSON : IEnumerable
	{
		//----------------------------------------------------------------------------------
		// Member Variables
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Hierarchy container to store the json structure and data
		/// </summary>
		protected Hierarchy m_Hierarchy = null;

		public string ToJSON()
		{

			return m_Hierarchy.ToJSON();
		}

		//----------------------------------------------------------------------------------
		// Properties
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Get the JSON hierarchy container, which contains the parsed json elements
		/// This is null if a successful parse has not been processed.
		/// </summary>
		public Hierarchy asJSONHierarchy
		{
			get{ return m_Hierarchy; }
		}

		//----------------------------------------------------------------------------------
		// Wrap the private hierarchy
		//----------------------------------------------------------------------------------

		//----------------------------------------------------------------------------------
		// Access via index

		/// <summary>
		/// Gets the Data found at the index,
		/// Dummy element returned if not found.
		/// </summary>
		/// <param name="index">Index of the element within the root object/array.</param>
		public Data this[int index]
		{
			get
			{
				return ( m_Hierarchy == null || index < 0 || index >= m_Hierarchy.Count ) ? 
					new Data(null) : 
					m_Hierarchy[index].Value;
			}
		}

		/// <summary>
		/// Gets the Data found at the index,
		/// Dummy element returned if not found.
		/// </summary>
		/// <returns>The Data object found.</returns>
		/// <param name="index">Index of the element within the root object/array.</param>
		public Data GetElement( int index )
		{
			return this[index];
		}

		/// <summary>
		/// Tries the Get Data element at the provided index
		/// </summary>
		/// <returns><c>true</c>, if get element was found, <c>false</c> otherwise.</returns>
		/// <param name="index">Index of the element within the root object/array.</param>
		/// <param name="data">Data reference to place the found element to.</param>
		public bool TryGetElement( int index, out Data data )
		{
			if( m_Hierarchy == null || index < 0 || index >= m_Hierarchy.Count )
			{
				data = null;
				return false;
			}
			else
			{
				data = m_Hierarchy[index].Value;
				return true;
			}
		}

		//----------------------------------------------------------------------------------
		// Access via key

		/// <summary>
		/// Gets the Data element found with the provided key,
		/// Dummy element returned if not found.
		/// </summary>
		/// <param name="key">Key name of the element to find.</param>
		public Data this[string key]
		{
			get
			{
				if( m_Hierarchy == null )
					return null;

				Element e = m_Hierarchy[key];
				return e == null ? new Data(key, null) : e.Value;
			}
		}

		/// <summary>
		/// Gets the Data element found with the provided key,
		/// Dummy element returned if not found.
		/// </summary>
		/// <returns>The element found.</returns>
		/// <param name="key">Key name of the element to find.</param>
		public Data GetElement( string key )
		{
			return this[key];
		}

		/// <summary>
		/// Tries the get element with the provided key.
		/// </summary>
		/// <returns><c>true</c>, if the element was found, <c>false</c> otherwise.</returns>
		/// <param name="key">Key name of the element to look for.</param>
		/// <param name="data">Data reference to place the found element to.</param>
		public bool TryGetElement( string key, out Data data )
		{
			if( m_Hierarchy == null )
			{
				data = null;
				return false;
			}
			Element e = m_Hierarchy[key];

			if( e == null )
			{
				data = null;
				return false;
			}

			data = e.Value;
			return true;
		}

		//----------------------------------------------------------------------------------
		// IEnumerable 
		//----------------------------------------------------------------------------------

		/// <summary>
		/// IEnumerable extention for foreach use
		/// </summary>
		/// <returns>The enumerator.</returns>
		public IEnumerator GetEnumerator()
		{
			if( m_Hierarchy == null )
				yield break;

			for( int i=0; i<m_Hierarchy.Count; ++i )
				yield return m_Hierarchy[i].Value;
		}

		//----------------------------------------------------------------------------------
		// Utilities
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Clear the json hierarchy
		/// </summary>
		public void Clear()
		{
			if( m_Hierarchy != null )
				m_Hierarchy.Clear();
		}

		//----------------------------------------------------------------------------------
		// Writing
		//----------------------------------------------------------------------------------

		public void CreateNew()
		{
			m_Hierarchy = new Hierarchy();
		}

		public void AddData( Data d )
		{
			if( m_Hierarchy == null )
				CreateNew();

			if( d.hierarchyNode != null )
				m_Hierarchy.Add( d.hierarchyNode );
			else
				d.hierarchyNode = m_Hierarchy.Add( d.key, d );
		}
	}
}
