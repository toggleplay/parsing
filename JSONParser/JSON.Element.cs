﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: This is a copy of the previously used TogglePlay.Generics.Hierarchy (See TogglePlay.Core package)
 * This is to allow this package to be used seperately from Core
 * 
 * Tasks:
 * - Lock the length of ToString() to prevent it from crashing in the 4.6 long log crash (editor)
 * - - Check to see if it only happens on some unity versions
 * - Check if IElementEnumerator works on windows phone and if there is an alternative
 * - Rename to node? to avoid confusion?
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

using IElementEnumerable = System.Collections.Generic.IEnumerable<TogglePlay.JSON.Element>;
using IElementEnumerator = System.Collections.Generic.IEnumerator<TogglePlay.JSON.Element>;

namespace TogglePlay
{

	public partial class JSON
	{
		/// <summary>
		/// Copy of TogglePlay.Generic.DynamicList but for just Element.
		/// Separate from the TogglePlay Core package (see file summary)
		/// </summary>
		public class ElementList : IElementEnumerable
		{
			//----------------------------------------------------------------------------------
			// Member Variables
			//----------------------------------------------------------------------------------

			/// <summary>
			/// The array to use and to change size of as the List grows
			/// </summary>
			protected Element[] array;

			/// <summary>
			/// The current length of the elements in the List
			/// </summary>
			private int length = 0;
			
			//----------------------------------------------------------------------------------
			// Utility Properties
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Gets the number of elements.
			/// </summary>
			/// <value>The number of active elements in the array.</value>
			public int Count
			{
				get{ return length; }
			}
			
			//----------------------------------------------------------------------------------
			// Enumerators
			//----------------------------------------------------------------------------------

			/// <summary>
			/// For system use
			/// </summary>
			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}
			
			/// <summary>
			/// Gets IEnumerator for use in foreach loops and IEnumerable<Element> function parameters
			/// </summary>
			/// <returns>Each element within the List as IEnumerator.</returns>
			public IElementEnumerator GetEnumerator()
			{
				if( array != null )
				{
					for( int i = 0; i < length; ++i )
					{
						yield return array[i];
					}
				}
				
				yield break;
			}
			
			//----------------------------------------------------------------------------------
			// Access Operators and Methods
			//----------------------------------------------------------------------------------
			
			/// <summary>
			/// Gets or sets the <see cref="TogglePlay.JSON+ElementList"/> with the specified index.
			/// Has no error checking.
			/// </summary>
			/// <param name="i">The index of the Element to get.</param>
			public Element this[int i]
			{
				get{ return array[i]; }
				set{ array[i] = value; }
			}

			/// <summary>
			/// Gets an array containing all elements within the List
			/// </summary>
			/// <returns>The array.</returns>
			public Element[] ToArray()
			{
				if( array == null || array.Length == 0 )
					return new Element[0];
				
				Element[] rtn = new Element[length];
				System.Array.Copy( array, 0, rtn, 0, length );
				
				return rtn;
			}

			/// <summary>
			/// Check to see if the List contains the specified item.
			/// </summary>
			/// <param name="item">Element to look for.</param>
			public bool Contains( Element item )
			{
				if( array == null )
					return false;
				
				for( int i = 0; i < length; ++i )
				{
					if( array[i].Equals( item ) )
						return true;
				}

				return false;
			}
			
			/// <summary>
			/// Get the index of an element stored.
			/// </summary>
			/// <returns>The index of the element, -1 if not found.</returns>
			/// <param name="item">Element to look for.</param>
			public int IndexOf( Element item )
			{
				if( array == null )
					return -1;
				
				for( int i = 0; i < length; ++i )
				{
					if( array[i].Equals( item ) )
						return i;
				}
				
				return -1;
			}
			
			//----------------------------------------------------------------------------------
			// Element Array Capacity
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Gets the capacity of the list until it needs to extend.
			/// or sets the capacity of the list, potentially cropping elements.
			/// </summary>
			/// <value>The capacity.</value>
			public int Capacity
			{
				get{ return array.Length; }
				set
				{
					Element[] newCap = new Element[value];
					if( array != null )
						System.Array.Copy( array, 0, newCap, 0, System.Math.Min( value, length ) );

					length = System.Math.Min( value, length );
					array = newCap;
				}
			}

			/// <summary>
			/// Doubles the capacity of the list with 32 being the starting amount.
			/// </summary>
			void ExpandCapacity()
			{
				Capacity = array == null ? 32 : System.Math.Max( array.Length << 1, 32 );
			}
			
			//----------------------------------------------------------------------------------
			// Clearing
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Clears the list of all values.
			/// </summary>
			/// <param name="clearMemory">If set to <c>true</c> resets the capacity of the list.</param>
			public void Clear( bool clearMemory = false )
			{
				length = 0;
				if( clearMemory )
					array = null;
			}
			
			//----------------------------------------------------------------------------------
			// Insertion
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Adds the specified element to the list
			/// </summary>
			/// <param name="item">Element to add.</param>
			public void Add( Element item )
			{
				if( array == null || length == array.Length )
					ExpandCapacity();
				
				array[length++] = item;
			}
			
			/// <summary>
			/// Inserts the Element item into the children of this element
			/// at index, and pushes all further children by +1 index
			/// </summary>
			/// <param name="index">Index to place item.</param>
			/// <param name="item">Element to place.</param>
			public void Insert( int index, Element item )
			{
				// make sure there is room
				if( array == null || length == array.Length )
					ExpandCapacity();
				
				if( index < length )
				{
					// push everything up
					for( int i = length; i > index; --i )
						array[i] = array[i - 1];
					
					// replace the position element
					array[index] = item;
					++length;
				}
				else
					Add( item );
			}
			
			//----------------------------------------------------------------------------------
			// Removal
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Remove the specified element.
			/// </summary>
			/// <returns>If the element was found and removed or not.</returns>
			/// <param name="item">Item to remove.</param>
			public bool Remove( Element item )
			{
				if (array != null)
				{
					for( int i = 0; i < length; ++i )
					{
						if( array[i].Equals(item) )
						{
							RemoveAt(i);
							return true;
						}
					}
				}
				return false;
			}

			/// <summary>
			/// Removes all elements that match the Predicate/Delegate.
			/// </summary>
			/// <returns>The amount of elements that were removed.</returns>
			/// <param name="match">Predicate to invoke.</param>
			public int RemoveAll( System.Predicate<Element> match )
			{
				int removed = 0;
				if( array != null )
				{
					for( int i = 0; i < length; ++i )
					{
						if( match.Invoke( array[i] ) )
						{
							removed++;
						}
						else if( removed > 0 )
						{
							// move down by removed count
							array[i-removed] = array[i];
						}
					}
				}
				
				// nullify the top 'removed'
				for( int i=length-1; i>length-1-removed; --i )
					array[i] = null;
				
				length -= removed;
				return removed;
			}

			/// <summary>
			/// Removes the element at index the specified index.
			/// </summary>
			/// <param name="index">Index of the element to remove.</param>
			public void RemoveAt( int index )
			{
				if( array == null || index >= length )
					return;
				
				--length;
				
				for( int i = index; i < length; ++i )
					array[i] = array[i + 1];
				
				array[length] = null;
			}
		}

		///<summary>
		///The hierarchy element defines a single element within the hierarchy
		///</summary>
		public class Element
		{
			/// <summary>
			/// The key name of this element
			/// </summary>
			protected string m_Key;

			/// <summary>
			/// The json data of this element
			/// </summary>
			protected JSON.Data m_Value;

			/// <summary>
			/// The parent element to this element
			/// </summary>
			protected Element m_Parent = null;

			/// <summary>
			/// Child elements list
			/// </summary>
			protected ElementList m_Children = null;


			//--------------------------------------------------------------=--------------------
			// Constructors
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Element"/> class.
			/// </summary>
			/// <param name="key">Element Key.</param>
			/// <param name="value">Element Value.</param>
			public Element( string key, JSON.Data value )
			{
				m_Key = key;
				m_Value = value;
				m_Children = new ElementList();
			}

			//----------------------------------------------------------------------------------
			// 
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Parent Element this Element is a child of
			/// </summary>
			public Element Parent
			{
				get{ return m_Parent; }
			}

			/// <summary>
			/// The number of child elements to this hierarchy element
			/// </summary>
			public int Count
			{
				get{ return m_Children.Count; }
			}

			/// <summary>
			/// The key name of this element.
			/// </summary>
			public string Key
			{
				get{ return m_Key; }
			}

			/// <summary>
			/// The json data contained within the element
			/// </summary>
			/// <value>The value.</value>
			public JSON.Data Value
			{
				get{ return m_Value; }
			}

			//----------------------------------------------------------------------------------
			// Element Access
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Gets an array of Elements for this elements children
			/// </summary>
			/// <returns>Children Elements.</returns>
			public Element[] ToArray()
			{
				return m_Children.ToArray();
			}

			/// <summary>
			/// Checks to see if a child with the specified key is found.
			/// </summary>
			/// <returns><c>true</c>, if child was found, <c>false</c> otherwise.</returns>
			/// <param name="key">Key name of child to look for.</param>
			public bool ContainsChildKey( string key )
			{
				for( int i=0; i<m_Children.Count; ++i )
				{
					if( m_Children[i].Key.Equals( key ) )
						return true;
				}
				
				return false;
			}

			/// <summary>
			/// Gets the child <see cref="TogglePlay.JSON+Element"/> at the specified index.
			/// </summary>
			/// <param name="index">Index of the child.</param>
			public Element this[int index]
			{
				get
				{
					if( index < 0 || index >= m_Children.Count )
						return null;
					return m_Children[index];
				}
			}

			/// <summary>
			/// Gets the child <see cref="TogglePlay.JSON+Element"/> at the specified index.
			/// </summary>
			/// <param name="index">Index of the child.</param>
			public Element GetElement( int index )
			{
				return this[index];
			}

			/// <summary>
			/// Gets the child <see cref="TogglePlay.JSON+Data"/> at the specified index.
			/// </summary>
			/// <param name="index">Index of the child.</param>
			public JSON.Data GetValue( int index )
			{
				return this[index].Value;
			}

			/// <summary>
			/// Gets the child <see cref="TogglePlay.JSON+Element"/> with the specified key name.
			/// If not found null element is returned.
			/// </summary>
			/// <param name="key">Key name of the child to look for.</param>
			public Element this[string key]
			{
				get
				{
					for( int i=0; i<m_Children.Count; ++i )
					{
						if( m_Children[i].Key.Equals( key ) )
							return m_Children[i];
					}
					return null;
				}
			}

			/// <summary>
			/// Gets the child <see cref="TogglePlay.JSON+Element"/> with the specified key name.
			/// If not found null element is returned.
			/// </summary>
			/// <param name="key">Key name of the child to look for.</param>
			public Element GetElement( string key )
			{
				return this[key];
			}

			/// <summary>
			/// Gets the child <see cref="TogglePlay.JSON+Data"/> with the specified key name.
			/// If not found null element is returned.
			/// </summary>
			/// <param name="key">Key name of the child to look for.</param>
			public JSON.Data GetValue( string key )
			{
				return this[key].Value;
			}

			/// <summary>
			/// Looks for a decendent, each child looking for a child with the next key in the string array keyPath.
			/// If no decendent can be found null Element is returned.
			/// </summary>
			/// <param name="keyPath">Key path string array, each string a key name of the next decendent.</param>
			public Element this[string[] keyPath]
			{
				get{ return GetElement( keyPath ); }
			}

			/// <summary>
			/// Looks for a decendent, each child looking for a child with the next key in the string array keyPath.
			/// If no decendent can be found null Element is returned.
			/// </summary>
			/// <returns>The decendent element if found, null otherwise</returns>
			/// <param name="keyPath">Key path string array, each string a key name of the next decendent.</param>
			public Element GetElement( string[] keyPath )
			{
				if( keyPath == null || keyPath.Length == 0 )
					return null;

				Element e = this;
				for( int i=0; i<keyPath.Length; ++i )
				{
					e = e[keyPath[i]];
					if( e == null )
						break;
				}
				
				return e;
			}


			//----------------------------------------------------------------------------------
			// Add Child functions
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Adds a new child element with the key and data provided.
			/// </summary>
			/// <param name="key">Key name of the new child element to add.</param>
			/// <param name="value">Data Value of the new child element to add.</param>
			public Element Add( string key, JSON.Data value )
			{
				if( ContainsChildKey( key ) )
				{
					// Key already exists within heirarchy nodes children
					return null;
				}

				Element childElement = new Element( key, value );
				childElement.m_Parent = this;
				value.hierarchyNode = childElement;

				m_Children.Add( childElement );
				return childElement;
			}

			/// <summary>
			/// Add a new child with the specified childElement.
			/// </summary>
			/// <param name="childElement">Child element to add.</param>
			public Element Add( Element childElement )
			{
				if( childElement == null || ContainsChildKey( childElement.Key ) )
				{
					Debug.LogError("Key already exists within heirarchy nodes children");
					return null;
				}

				if( childElement.m_Parent != null && childElement.m_Parent != this )
					childElement.m_Parent.Remove( childElement.Key );

				childElement.m_Parent = this;
				m_Children.Add( childElement );

				return childElement;
			}

			/// <summary>
			/// Inserts a new child element with the specified key and data provided, at the specified index.
			/// </summary>
			/// <param name="index">Child index to insert to new element amongst the children.</param>
			/// <param name="key">Key name of the child element to insert.</param>
			/// <param name="value">Data value of the child element to insert.</param>
			public Element Insert( int index, string key, JSON.Data value )
			{
				if( ContainsChildKey( key ) )
					return null;

				Element childElement = new Element( key, value );
				childElement.m_Parent = this;
				value.hierarchyNode = childElement;

				m_Children.Insert( index, childElement );
				return childElement;
			}

			//----------------------------------------------------------------------------------
			// Remove Child Functions
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Remove the element if found with the specified key.
			/// </summary>
			/// <param name="key">Key name of the child to remove.</param>
			/// <returns>true if the child was found and removed, false if it was not found.</returns>
			public bool Remove( string key )
			{
				int containedAtIndex = -1;
				for( int i=0; i<m_Children.Count; ++i )
				{
					if( m_Children[i].Key.Equals( key ) )
					{
						containedAtIndex = i;
						break;
					}
				}

				if( containedAtIndex == -1 )
					return false;

				m_Children.RemoveAt( containedAtIndex );
				return true;
			}

			/// <summary>
			/// Removes all children that the invoke of match returns true for.
			/// </summary>
			/// <returns>The number of children removed.</returns>
			/// <param name="match">Match predicate to determine which to remove.</param>
			public int RemoveAll( System.Predicate<Element> match )
			{
				return m_Children.RemoveAll( match );
			}

			//----------------------------------------------------------------------------------
			// Other
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Formats the keys and values stored and returns the string
			/// </summary>
			public override string ToString()
			{
				string self = string.Format( "[Element: Key={0}, Value={1}, ", Key, Value );

				for( int i=0; i<m_Children.Count; ++i )
					self += "Child"+i.ToString() + "=" + m_Children[i].ToString();

				return self + "]";
			}
		}

	}

}
