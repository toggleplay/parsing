/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: This is the partial part of the JSON class. In this file, the code of the
 * Data object which is for each individual element in the json
 * 
 * Tasks:
 * - ToJSONString function
 * - check Window Phone on System.Collections.Generic.IEnumerable<Data> and see about an alternative
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

namespace TogglePlay
{

	public partial class JSON : IEnumerable
	{
		//----------------------------------------------------------------------------------
		// Data class, used within the hierarchy for storing the json data
		//----------------------------------------------------------------------------------

		///	<summary>
		/// Data class is used to store and manage data read from the json parse
		/// </summary>
		[System.Serializable]
		public class Data : IEnumerable
		{
			public string ToJSON()
			{
				if( m_Type == EType.UNKNOWN )
				{
					if( FindType() == EType.UNKNOWN )
						return null;
				}

				if( m_Type == EType.CASTFAIL )
					return null;

				if( hierarchyNode == null )
				{
					Debug.LogError( "No Hierarchy node : " + key );
					return "";
				}

				bool arrayChild =
					( hierarchyNode == null 
				    || hierarchyNode.Parent == null
					|| hierarchyNode.Parent.Value == null ) ? false :
					hierarchyNode.Parent.Value.isArray;

				switch( m_Type )
				{
				case EType.OBJECT:
				{
					System.Text.StringBuilder str = new System.Text.StringBuilder( arrayChild ? "{" : ("\""+key+"\":{") );
					int c = hierarchyNode.Count;
					for( int i=0; i<c; i++ )
					{
						Element e = hierarchyNode[i];
						if( e == null )
						{
							Debug.LogError( "Could not find : " + c + "  count=" + c );
							continue;
						}
						if( hierarchyNode[i].Value == null )
							continue;
						str.Append( hierarchyNode[i].Value.ToJSON() );
						if( i < c-1 )
							str.Append( ',' );
					}
					str.Append( "}" );
					return str.ToString();

				}
				case EType.ARRAY:
				{
					System.Text.StringBuilder str = new System.Text.StringBuilder( arrayChild ? "[" : ("\""+key+"\":[") );
					int c = hierarchyNode.Count;
					for( int i=0; i<c; ++i )
					{
						if( hierarchyNode[i].Value == null )
							continue;
						str.Append( hierarchyNode[i].Value.ToJSON() );
						if( i < c-1 )
							str.Append( ',' );
					}
					str.Append( "]" );
					return str.ToString();
				}
				case EType.STRING:
				{
					if( arrayChild )
						return "\""+m_Value+"\"";
					return string.Format( "\"{0}\":\"{1}\"", new string[]{ key, m_Value } );
				}
				case EType.INT:
				{
					if( arrayChild )
						return m_Int.ToString();
					return string.Format( "\"{0}\":{1}", new string[]{ key, m_Int.ToString() } );
				}
				case EType.FLOAT:
				{
					if( arrayChild )
						return m_Float.ToString();
					return string.Format( "\"{0}\":{1}", new string[]{ key, m_Float.ToString() } );
				}
				case EType.BOOL:
				{
					if( arrayChild )
						return m_Bool.ToString();
					return string.Format( "\"{0}\":{1}", new string[]{ key, m_Bool.ToString() } );
				}
				case EType.NULL:
				{
					if( arrayChild )
						return "null";
					return "\"" + key + "\":null";
				}
				}
				return null;
			}

			//----------------------------------------------------------------------------------
			// Member Declarations
			//----------------------------------------------------------------------------------

			///<summary>
			/// EType enum used to store what type of data this instance has stored.
			/// </summary>
			public enum EType
			{
				UNKNOWN = 0,
				OBJECT,
				ARRAY,
				STRING,
				INT,
				FLOAT,
				BOOL,
				NULL,
				CASTFAIL
			}

			//----------------------------------------------------------------------------------
			// Public Variables
			//----------------------------------------------------------------------------------

			/// <summary>
			/// The key or name of the json element, (arrayed objects will have no key)
			/// </summary>
			public string key = null;

			/// <summary>
			/// The generic hierarchy element that this data belongs to.
			/// </summary>
			public Element hierarchyNode = null;

			//----------------------------------------------------------------------------------
			// Private variables
			//----------------------------------------------------------------------------------

			/// <summary>
			/// The data type stored within this element.
			/// </summary>
			protected EType m_Type = EType.UNKNOWN;

			/// <summary>
			/// string based value acquired from the json.
			/// </summary>
			protected string m_Value = "";

			/// <summary>
			/// Stored bool value.
			/// </summary>
			protected bool m_Bool = false;

			/// <summary>
			/// Stored float value.
			/// </summary>
			protected float m_Float = 0f;

			/// <summary>
			/// Stored int value.
			/// </summary>
			protected int m_Int = 0;


			//----------------------------------------------------------------------------------
			// Constructors
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class.
			/// Default settings with UNKNOWN type
			/// </summary>
			public Data()
			{
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class.
			/// if valid string provided Sets to STRING type, with stringValue as its value.
			/// if null is sent as stringValue parameter, Sets to NULL types with no value.
			/// </summary>
			/// <param name="stringValue">String value.</param>
			public Data( string stringValue )
			{
				if( stringValue == null )
					SetNull();
				else
					SetValue( stringValue );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class.
			/// Sets to a FLOAT type with the floatValue as its value.
			/// </summary>
			/// <param name="floatValue">Float value.</param>
			public Data( float floatValue )
			{
				SetValue( floatValue );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class.
			/// Sets to a INT type with intValue as its value.
			/// </summary>
			/// <param name="intValue">Int value.</param>
			public Data( int intValue )
			{
				SetValue( intValue );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class.
			/// Sets to a BOOL type with boolValue as its value.
			/// </summary>
			/// <param name="boolValue">If set to <c>true</c> bool value.</param>
			public Data( bool boolValue )
			{
				SetValue( boolValue );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class,
			/// with the key value for reference to the data element.
			/// if valid string provided Sets to STRING type, with stringValue as its value.
			/// if null is sent as stringValue parameter, Sets to NULL types with no value.
			/// </summary>
			/// <param name="key">Key to the element</param>
			/// <param name="stringValue">String value.</param>
			public Data( string key, string stringValue )
			{
				this.key = key;
				if( stringValue == null )
					SetNull();
				else
					SetValue( stringValue );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class,
			/// with the key value for reference to the data element.
			/// Sets to a FLOAT type with the floatValue as its value.
			/// </summary>
			/// <param name="key">Key to the element</param>
			/// <param name="floatValue">Float value.</param>
			public Data( string key, float floatValue )
			{
				this.key = key;
				SetValue( floatValue );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class,
			/// with the key value for reference to the data element.
			/// Sets to a INT type with intValue as its value.
			/// </summary>
			/// <param name="key">Key to the element</param>
			/// <param name="intValue">Int value.</param>
			public Data( string key, int intValue )
			{
				this.key = key;
				SetValue( intValue );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Data"/> class,
			/// with the key value for reference to the data element.
			/// Sets to a BOOL type with boolValue as its value.
			/// </summary>
			/// <param name="key">Key to the element</param>
			/// <param name="boolValue">If set to <c>true</c> bool value.</param>
			public Data( string key, bool boolValue )
			{
				this.key = key;
				SetValue( boolValue );
			}


			//----------------------------------------------------------------------------------
			// Properties (Types)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Gets or sets a value indicating whether this <see cref="TogglePlay.JSON+Data"/> is an array type container.
			/// </summary>
			/// <value><c>true</c> if is array; otherwise, <c>false</c>.</value>
			public bool isArray
			{
				get{ return m_Type == EType.ARRAY; }
				set{ m_Type = ( value ? EType.ARRAY : m_Type ); }
			}

			/// <summary>
			/// Gets or sets a value indicating whether this <see cref="TogglePlay.JSON+Data"/> is object type container.
			/// </summary>
			/// <value><c>true</c> if is object; otherwise, <c>false</c>.</value>
			public bool isObject
			{
				get{ return m_Type == EType.OBJECT; }
				set{ m_Type = ( value ? EType.OBJECT : m_Type ); }
			}

			/// <summary>
			/// Gets a value indicating whether this <see cref="TogglePlay.JSON+Data"/> is an array or an object type container.
			/// </summary>
			/// <value><c>true</c> if is an array or object; otherwise, <c>false</c>.</value>
			public bool isContainer
			{
				get{ return m_Type == EType.ARRAY || m_Type == EType.OBJECT; }
			}

			/// <summary>
			/// Gets a value indicating whether this <see cref="TogglePlay.JSON+Data"/> is number type of either int or float.
			/// </summary>
			/// <value><c>true</c> if is a number value; otherwise, <c>false</c>.</value>
			public bool isNumber
			{
				get
				{
					if( m_Type == EType.UNKNOWN )
						FindType();

					return m_Type == EType.INT || m_Type == EType.FLOAT;
				}
			}

			/// <summary>
			/// Gets a value indicating whether this <see cref="TogglePlay.JSON+Data"/> is null type.
			/// </summary>
			/// <value><c>true</c> if is null type; otherwise, <c>false</c>.</value>
			public bool isNull
			{
				get
				{
					if( m_Type == EType.UNKNOWN )
						return FindType() == EType.NULL;

					return m_Type == EType.NULL;
				}
			}

			/// <summary>
			/// Gets a value indicating whether this <see cref="TogglePlay.JSON+Data"/> is string type.
			/// </summary>
			/// <value><c>true</c> if is string type; otherwise, <c>false</c>.</value>
			public bool isString
			{
				get
				{
					if( m_Type == EType.UNKNOWN )
						return FindType() == EType.STRING;

					return m_Type == EType.STRING;
				}
			}

			/// <summary>
			/// Gets a value indicating whether this <see cref="TogglePlay.JSON+Data"/> is a bool type.
			/// </summary>
			/// <value><c>true</c> if is bool type; otherwise, <c>false</c>.</value>
			public bool isBool
			{
				get
				{
					if( m_Type == EType.UNKNOWN )
						return FindType() == EType.BOOL;

					return m_Type == EType.BOOL;
				}
			}

			//----------------------------------------------------------------------------------
			// Methods (Initialisation)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Initialise the data from a string builder
			/// </summary>
			/// <param name="stringBuilder">String builder to use for data value.</param>
			public void Init( System.Text.StringBuilder stringBuilder )
			{
				if( m_Type == EType.ARRAY || m_Type == EType.OBJECT || stringBuilder.Length == 0 )
					return;

				m_Value = stringBuilder.ToString();
				if( JSON.checkTypeOnSet )
				{
					// try casting the string to a type
					FindType();
				}
			}

			/// <summary>
			/// Initialise the data directly from a string
			/// </summary>
			/// <param name="str">String to use for data value.</param>
			public void Init( string str )
			{
				if( m_Type == EType.ARRAY || m_Type == EType.OBJECT )
					return;

				m_Value = str;
				if( JSON.checkTypeOnSet )
				{
					// try casting the string to a type
					FindType();
				}
			}

			/// <summary>
			/// Initialise the data using a char array
			/// </summary>
			/// <param name="charArray">Char array to take the value from.</param>
			/// <param name="length">Number of chars to take from the char array (from index 0).</param>
			public void Init( char[] charArray, int length )
			{
				if( m_Type == EType.ARRAY || m_Type == EType.OBJECT )
					return;

				m_Value = new string( charArray, 0, length );
				if( JSON.checkTypeOnSet )
				{
					// try casting the string to a type
					FindType();
				}
			}

			/// <summary>
			/// If a type for the value has not yet been set, will try to cast the string value
			/// to the most appropriate data type
			/// </summary>
			/// <returns>The type.</returns>
			EType FindType()
			{
				if( m_Type != EType.UNKNOWN )
					return m_Type;

				if( string.IsNullOrEmpty( m_Value ) )
				{
					m_Type = EType.NULL;
				}
				else if( string.Compare( m_Value, "true", true ) == 0 )
				{
					m_Type = EType.BOOL;
					m_Bool = true;
				}
				else if( string.Compare( m_Value, "false", true ) == 0 )
				{
					m_Type = EType.BOOL;
					m_Bool = false;
				}
				else if( string.Compare( m_Value, "null", true ) == 0 )
				{
					m_Type = EType.NULL;
				}
				else
				{
					// if it fails to cast to a number then it must be a string
					if( TryCastToNumberType() == false )
						m_Type = EType.STRING;
				}

				return m_Type;
			}

			/// <summary>
			/// Tries to cast the string value to a number, considered as a float if a decimal point is found
			/// and can be successfully cast to a number
			/// </summary>
			/// <returns><c>true</c>, if value was successfully cast to a number, <c>false</c> otherwise.</returns>
			bool TryCastToNumberType()
			{
				// presume its a number
				if( float.TryParse( Value, out m_Float ) == false )
				{
					// occurs when it should be a number type but contains invalid data, e.g. 23p9   (contains the letter p)
					m_Type = EType.CASTFAIL;
					return false;
				}
				else
				{
					m_Int = (int)m_Float;

					// check for float decimel
					for( int i=0; i<m_Value.Length; ++i )
					{
						if( m_Value[i] == '.' )
						{
							m_Type = EType.FLOAT;
							return true;
						}
					}

					// if it wasnt found to be a float then it must be an int
					m_Type = EType.INT;
					return true;
				}
			}


			//----------------------------------------------------------------------------------
			// Methods (Assigning new values)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Set the data value to a string.
			/// </summary>
			/// <param name="str">String to set to.</param>
			public void SetValue( string str )
			{
				m_Type = EType.STRING;
				m_Value = str;
			}

			/// <summary>
			/// Sets the data value to a float.
			/// </summary>
			/// <param name="f">Float to set to.</param>
			public void SetValue( float f )
			{
				m_Type = EType.FLOAT;
				m_Value = f.ToString();
				m_Float = f;
				m_Int = (int)f;
			}

			/// <summary>
			/// Sets the data value to an int.
			/// </summary>
			/// <param name="i">Int to set to.</param>
			public void SetValue( int i )
			{
				m_Type = EType.INT;
				m_Value = i.ToString();
				m_Int = i;
				m_Float = (float)i;
			}

			/// <summary>
			/// Sets the data value to a bool
			/// </summary>
			/// <param name="b">bool to set as</param>
			public void SetValue( bool b )
			{
				m_Type = EType.BOOL;
				m_Value = b.ToString();
				m_Bool = b;
			}

			/// <summary>
			/// Sets the data to 'null'
			/// </summary>
			public void SetNull()
			{
				m_Type = EType.NULL;
				m_Value = "null";
			}

			/// <summary>
			/// Sets this element to an Object type and adds children for x and y
			/// </summary>
			/// <param name="v2">Vector2 instance to use for the child values.</param>
			public void SetValue( Vector2 v2 )
			{
				m_Type = EType.OBJECT;
				
				this["x"].SetValue(v2.x);
				this["y"].SetValue(v2.y);
			}

			/// <summary>
			/// Sets this element to an Object type and adds children for x,y and z
			/// </summary>
			/// <param name="v3">Vector3 instance to use for the child values</param>
			public void SetValue( Vector3 v3 )
			{
				m_Type = EType.OBJECT;

				this["x"].SetValue( v3.x );
				this["y"].SetValue( v3.y );
				this["z"].SetValue( v3.z );
			}

			/// <summary>
			/// Sets this element to an Object type and adds children for x,y,z and w
			/// </summary>
			/// <param name="v4">Vector4 instance to use for the child values</param>
			public void SetValue( Vector4 v4 )
			{
				m_Type = EType.OBJECT;
				
				this["x"].SetValue( v4.x );
				this["y"].SetValue( v4.y );
				this["z"].SetValue( v4.z );
				this["w"].SetValue( v4.w );
			}

			/// <summary>
			/// Sets this element to an Object type and adds children for r,g,b and a
			/// </summary>
			/// <param name="c">Color instance to use for the child values.</param>
			public void SetValue( Color c )
			{
				m_Type = EType.OBJECT;
				
				this["r"].SetValue( c.r );
				this["g"].SetValue( c.g );
				this["b"].SetValue( c.b );
				this["a"].SetValue( c.a );
			}


			//----------------------------------------------------------------------------------
			// Properties (Standard types)
			//----------------------------------------------------------------------------------
			
			/// <summary>
			/// Gets the type of data stored.
			/// </summary>
			/// <value>The type of data stored for this element of the json document parsed.</value>
			public EType Type
			{
				get
				{
					// will check to see if it needs to be found, and send back the result
					return FindType();
				}
			}

			/// <summary>
			/// Gets the string of the Value assigned to the data
			/// </summary>
			/// <value>The string value from the json document.</value>
			public string Value
			{
				get{ return m_Value; }
			}

			/// <summary>
			/// Gets the string of the Value assigned to the data
			/// </summary>
			/// <value>The string value from the json document.</value>
			public string AsString
			{
				get{ return m_Value; }
			}

			/// <summary>
			/// Returns an integer value cast from the json string data
			/// </summary>
			public int AsInt
			{
				get
				{
					int i = 0;
					TryGetInt( out i );
					return i;
				}
			}

			/// <summary>
			/// Returns a float value cast from the json string data
			/// </summary>
			public float AsFloat
			{
				get
				{
					float f = 0f;
					TryGetFloat( out f );
					return f;
				}
			}

			/// <summary>
			/// Returns a bool cast from the json string data
			/// </summary>
			public bool AsBool
			{
				get
				{
					bool b = false;
					TryGetBool( out b );
					return b;
				}
			}

			//----------------------------------------------------------------------------------
			// Methods (Standard Types)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Tries to assign an integer value to i if the value of the data can be cast to an int.
			/// If it fails to cast then 0 will be assigned to i instead.
			/// </summary>
			/// <returns><c>true</c>, if the int was assigned successfully, <c>false</c> otherwise.</returns>
			/// <param name="i">The integer to assign as the value.</param>
			public bool TryGetInt( out int i )
			{
				return TryGetInt( out i, 0 );
			}

			/// <summary>
			/// Tries to assign an integer value to i if the value of the data can be cast to an int.
			/// If it fails to cast then the provided defaultValue will be assigned to i instead.
			/// </summary>
			/// <returns><c>true</c>, if the int was assigned successfully, <c>false</c> otherwise.</returns>
			/// <param name="i">The integer to assign as the value.</param>
			/// <param name="defaultValue">Default value to assign if not found.</param>
			public bool TryGetInt( out int i, int defaultValue )
			{
				if( m_Type == EType.INT || m_Type == EType.FLOAT )
				{
					i = m_Int;
					return true;
				}
				else if( m_Type == EType.STRING )
				{
					// try casting it to an int
					return int.TryParse( m_Value, out i );
				}
				else if( m_Type == EType.UNKNOWN )
				{
					FindType();
					
					if( m_Type == EType.INT || m_Type == EType.FLOAT )
					{
						i = m_Int;
						return true;
					}
					else if( m_Type == EType.STRING )
					{
						// try casting it to an int
						return int.TryParse( m_Value, out i );
					}
				}
				
				i = defaultValue;
				return false;
			}

			/// <summary>
			/// Tries to assign a float value to f if the value of the data can be cast to an float.
			/// If it fails to cast then 0 will be assigned to f instead.
			/// </summary>
			/// <returns><c>true</c>, if the float was assigned successfully, <c>false</c> otherwise.</returns>
			/// <param name="f">The float to assign as the value.</param>
			public bool TryGetFloat( out float f )
			{
				return TryGetFloat( out f, 0 );
			}

			/// <summary>
			/// Tries to assign a float value to f if the value of the data can be cast to an float.
			/// If it fails to cast then the provided defaultValue will be assigned to f instead.
			/// </summary>
			/// <returns><c>true</c>, if the float was assigned successfully, <c>false</c> otherwise.</returns>
			/// <param name="f">The float to assign as the value.</param>
			/// <param name="defaultValue">Default value to assign if not found.</param>
			public bool TryGetFloat( out float f, float defaultValue )
			{
				if( m_Type == EType.INT || m_Type == EType.FLOAT )
				{
					f = m_Float;
					return true;
				}
				else if( m_Type == EType.STRING )
				{
					// try casting it to a float
					return float.TryParse( m_Value, out f );
				}
				else if( m_Type == EType.UNKNOWN )
				{
					FindType();
					
					if( m_Type == EType.INT || m_Type == EType.FLOAT )
					{
						f = m_Float;
						return true;
					}
					else if( m_Type == EType.STRING )
					{
						// try casting it to a float
						return float.TryParse( m_Value, out f );
					}
				}
				
				f = defaultValue;
				return false;
			}

			/// <summary>
			/// Tries to assign a bool value to b if the value of the data can be cast to a bool.
			/// If it fails to cast then false will be assigned to b instead.
			/// </summary>
			/// <returns><c>true</c>, if the bool was assigned successfully, <c>false</c> otherwise.</returns>
			/// <param name="b">The bool to assign as the value.</param>
			public bool TryGetBool( out bool b )
			{
				return TryGetBool( out b, false );
			}

			/// <summary>
			/// Tries to assign a bool value to b if the value of the data can be cast to a bool.
			/// If it fails to cast then the provided defaultValue will be assigned to b instead.
			/// </summary>
			/// <returns><c>true</c>, if the bool was assigned successfully, <c>false</c> otherwise.</returns>
			/// <param name="b">The bool to assign as the value.</param>
			/// <param name="defaultValue">Default value to assign if not found.</param>
			public bool TryGetBool( out bool b, bool defaultValue )
			{
				if( m_Type == EType.BOOL )
				{
					b = m_Bool;
					return true;
				}
				else if( m_Type == EType.STRING )
				{
					// try casting it to a bool
					return bool.TryParse( m_Value, out b );
				}
				else if( m_Type == EType.INT )
				{
					b = m_Int > 0;
					return true;
				}
				else if( m_Type == EType.UNKNOWN )
				{
					FindType();
					
					if( m_Type == EType.BOOL )
					{
						b = m_Bool;
						return true;
					}
					else if( m_Type == EType.STRING )
					{
						// try casting it to a bool
						return bool.TryParse( m_Value, out b );
					}
					else if( m_Type == EType.INT )
					{
						b = m_Int > 0;
						return true;
					}
				}
				
				b = defaultValue;
				return false;
			}

			//----------------------------------------------------------------------------------
			// Properties (Unity values for valid child elements)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// If the data is a json object and has x and y children to read as the axis of a Vector2 class
			/// </summary>
			/// <value>Vector2 instance returned from the children data.</value>
			public Vector2 AsVector2
			{
				get
				{
					if( ! isObject )
						return Vector2.zero;
					
					// search for the x,y values within the children
					Vector2 v = Vector2.zero;
					bool xSet = false, ySet = false;
					
					for( int i=0; i<Count; ++i )
					{
						if( hierarchyNode[i].Value.key.Length != 1 )
							continue;
						
						if( hierarchyNode[i].Value.key == "x" || hierarchyNode[i].Value.key == "X" )
						{
							v.x = hierarchyNode[i].Value.AsFloat;
							xSet = true;
						}
						else if( hierarchyNode[i].Value.key == "y" || hierarchyNode[i].Value.key == "Y" )
						{
							v.y = hierarchyNode[i].Value.AsFloat;
							ySet = true;
						}
						
						if( xSet && ySet )
							break;
					}
					
					return v;
				}
			}

			/// <summary>
			/// If the data is a json object and has x,y and z children to read as the axis of a Vector3 class
			/// </summary>
			/// <value>Vector3 instance returned from the children data.</value>
			public Vector3 AsVector3
			{
				get
				{
					if( ! isObject )
						return Vector3.zero;
					
					// search for the x,y,z values within the children
					Vector3 v = Vector3.zero;
					bool xSet = false, ySet = false, zSet = false;
					
					for( int i=0; i<Count; ++i )
					{
						if( hierarchyNode[i].Value.key.Length != 1 )
							continue;
						
						if( hierarchyNode[i].Value.key == "x" || hierarchyNode[i].Value.key == "X" )
						{
							v.x = hierarchyNode[i].Value.AsFloat;
							xSet = true;
						}
						else if( hierarchyNode[i].Value.key == "y" || hierarchyNode[i].Value.key == "Y" )
						{
							v.y = hierarchyNode[i].Value.AsFloat;
							ySet = true;
						}
						else if( hierarchyNode[i].Value.key == "z" || hierarchyNode[i].Value.key == "Z" )
						{
							v.z = hierarchyNode[i].Value.AsFloat;
							zSet = true;
						}
						
						if( xSet && ySet && zSet )
							break;
					}
					
					return v;
				}
			}

			/// <summary>
			/// If the data is a json object and has x,y,z and w children to read as the axis of a Vector4 class
			/// </summary>
			/// <value>Vector4 instance returned from the children data.</value>
			public Vector4 AsVector4
			{
				get
				{
					if( ! isObject )
						return Vector4.zero;
					
					// search for the x,y,z values within the children
					Vector4 v = Vector4.zero;
					bool xSet = false, ySet = false, zSet = false, wSet = false;
					
					for( int i=0; i<Count; ++i )
					{
						if( hierarchyNode[i].Value.key.Length != 1 )
							continue;
						
						if( hierarchyNode[i].Value.key == "x" || hierarchyNode[i].Value.key == "X" )
						{
							v.x = hierarchyNode[i].Value.AsFloat;
							xSet = true;
						}
						else if( hierarchyNode[i].Value.key == "y" || hierarchyNode[i].Value.key == "Y" )
						{
							v.y = hierarchyNode[i].Value.AsFloat;
							ySet = true;
						}
						else if( hierarchyNode[i].Value.key == "z" || hierarchyNode[i].Value.key == "Z" )
						{
							v.z = hierarchyNode[i].Value.AsFloat;
							zSet = true;
						}
						else if( hierarchyNode[i].Value.key == "w" || hierarchyNode[i].Value.key == "W" )
						{
							v.w = hierarchyNode[i].Value.AsFloat;
							wSet = true;
						}
						
						if( xSet && ySet && zSet && wSet )
							break;
					}
					
					return v;
				}
			}

			/// <summary>
			/// If the data is a json object and has r,g,b and a children to read as the axis of a Color class
			/// </summary>
			/// <value>Color instance returned from the children data.</value>
			public Color AsColor
			{
				get
				{
					if( ! isObject )
						return Color.white;
					
					// search for the r,g,b,a values within the children
					Color c = Color.white;
					bool rSet = false, gSet = false, bSet = false, aSet = false;

					for( int i=0; i<Count; ++i )
					{
						switch( hierarchyNode[i].Value.key.Length )
						{
						case 1:
							if( hierarchyNode[i].Value.key == "r" || hierarchyNode[i].Value.key == "R" )
							{
								c.r = hierarchyNode[i].Value.AsFloat;
								rSet = true;
							}
							else if( hierarchyNode[i].Value.key == "g" || hierarchyNode[i].Value.key == "G" )
							{
								c.g = hierarchyNode[i].Value.AsFloat;
								gSet = true;
							}
							else if( hierarchyNode[i].Value.key == "b" || hierarchyNode[i].Value.key == "B" )
							{
								c.b = hierarchyNode[i].Value.AsFloat;
								bSet = true;
							}
							else if( hierarchyNode[i].Value.key == "a" || hierarchyNode[i].Value.key == "A" )
							{
								c.a = hierarchyNode[i].Value.AsFloat;
								aSet = true;
							}
							break;

						case 3:
							if( string.Compare( "red", hierarchyNode[i].Value.key, true ) == 0 )
							{
								c.r = hierarchyNode[i].Value.AsFloat;
								rSet = true;
							}
							break;

						case 4:
							if( string.Compare( "blue", hierarchyNode[i].Value.key, true ) == 0 )
							{
								c.b = hierarchyNode[i].Value.AsFloat;
								bSet = true;
							}
							break;

						case 5:
							if( string.Compare( "green", hierarchyNode[i].Value.key, true ) == 0 )
							{
								c.g = hierarchyNode[i].Value.AsFloat;
								gSet = true;
							}
							else if( string.Compare( "alpha", hierarchyNode[i].Value.key, true ) == 0 )
							{
								c.a = hierarchyNode[i].Value.AsFloat;
								aSet = true;
							}
							break;
						}

						if( rSet && gSet && bSet && aSet )
							break;
					}
					
					return c;
				}
			}


			//----------------------------------------------------------------------------------
			// Reflection class/struct
			//----------------------------------------------------------------------------------
			
			/// <summary>
			/// Finds child nodes within the json that match the field names
			/// or keyName attributes of the fields in the class/struct
			/// </summary>
			/// <returns>The object created from the data.</returns>
			/// <param name="objectType">Object type.</param>
			public object FromObject( System.Type objectType )
			{
				if( isArray )
					return null;

				object instance = System.Activator.CreateInstance( objectType );
				
				System.Type stringT = typeof( string );
				System.Type intT = typeof( int );
				System.Type floatT = typeof( float );
				System.Type doubleT = typeof( double );
				System.Type boolT = typeof( bool );

				System.Reflection.FieldInfo[] fields = objectType.GetFields();

				foreach( System.Reflection.FieldInfo field in fields )
				{
					if( field.IsStatic )
						continue;

					string jsonName = field.Name;
					System.Type fieldType = field.FieldType;

					KeyName[] attributes = field.GetCustomAttributes( typeof(KeyName), true ) as KeyName[];
					for( int i=0; i<attributes.Length; ++i )
					{
						// Key Name attribute found, use that instead
						if( hierarchyNode.ContainsChildKey( attributes[i].keyName ) )
						{
							jsonName = attributes[i].keyName;
							break;
						}
					}

					Data child = this[jsonName];
					if( child == null )
						continue;

					if( fieldType.IsArray )
					{
						if( child.isArray == false )
						{
							Debug.LogError( "Class field :" + field.Name + " is an array but not the json child :" + child.key );
							// move onto next
							continue;
						}
						
						System.Type elementType = fieldType.GetElementType();
						System.Array array = System.Array.CreateInstance( elementType, child.Count );
						
						for( int i=0; i<child.Count; ++i )
						{
							if( elementType == stringT )
								array.SetValue( child[i].AsString, i );
							else if( elementType == intT )
								array.SetValue( child[i].AsInt, i );
							else if( elementType == floatT )
								array.SetValue( child[i].AsFloat, i );
							else if( elementType == boolT )
								array.SetValue( child[i].AsBool, i );
							else if( elementType == doubleT )
								array.SetValue( (double)child[i].AsFloat, i );
							else
							{
								// try and get any sub values for the unknown type,
								object n = child[i].FromObject( elementType );
								if( n != null )
									array.SetValue( n, i );
							}
						}
						
						field.SetValue( instance, (object)array );
						
						continue;
					}
					
					if( fieldType == stringT )
						field.SetValue( instance, child.AsString );
					else if( fieldType == intT )
						field.SetValue( instance, child.AsInt );
					else if( fieldType == floatT )
						field.SetValue( instance, child.AsFloat );
					else if( fieldType == boolT )
						field.SetValue( instance, child.AsBool );
					else if( fieldType == doubleT )
						field.SetValue( instance, (double)child.AsFloat );
					else
					{
						// try and get any sub values for the unknown type,
						object subObject = child.FromObject( fieldType );
						if( subObject != null )
							field.SetValue( instance, subObject );
					}
				}
				return instance;
			}

			/// <summary>
			/// Finds child nodes within the json that match the field names
			/// or keyName attributes of the fields in the class/struct
			/// </summary>
			/// <returns>The object created from the data.</returns>
			/// <typeparam name="T">The type parameter.</typeparam>
			public T FromObject<T>()
			{
				if( isArray )
					return default(T);

				object instance = System.Activator.CreateInstance<T>();

				System.Type stringT = typeof( string );
				System.Type intT = typeof( int );
				System.Type floatT = typeof( float );
				System.Type doubleT = typeof( double );
				System.Type boolT = typeof( bool );

				System.Type objectType = typeof( T );
				System.Reflection.FieldInfo[] fields = objectType.GetFields();

				foreach( System.Reflection.FieldInfo field in fields )
				{
					if( field.IsStatic )
						continue;

					string jsonName = field.Name;
					System.Type fieldType = field.FieldType;

					KeyName[] attributes = field.GetCustomAttributes( typeof(KeyName), true ) as KeyName[];
					for( int i=0; i<attributes.Length; ++i )
					{
						// Key Name attribute found, use that instead
						if( hierarchyNode.ContainsChildKey( attributes[i].keyName ) )
						{
							jsonName = attributes[i].keyName;
							break;
						}
					}

					Data child = this[jsonName];
					if( child == null )
						continue;

					if( fieldType.IsArray )
					{
						if( child.isArray == false )
						{
							Debug.LogError( "Class field :" + field.Name + " is an array but not the json child :" + child.key );
							// move onto next
							continue;
						}

						System.Type elementType = fieldType.GetElementType();
						System.Array array = System.Array.CreateInstance( elementType, child.Count );

						for( int i=0; i<child.Count; ++i )
						{
							if( elementType == stringT )
								array.SetValue( child[i].AsString, i );
							else if( elementType == intT )
								array.SetValue( child[i].AsInt, i );
							else if( elementType == floatT )
								array.SetValue( child[i].AsFloat, i );
							else if( elementType == boolT )
								array.SetValue( child[i].AsBool, i );
							else if( elementType == doubleT )
								array.SetValue( (double)child[i].AsFloat, i );
							else
							{
								// try and get any sub values for the unknown type,
								object subObject = child[i].FromObject( elementType );
								if( subObject != null )
									array.SetValue( subObject, i );
							}
						}

						field.SetValue( instance, (object)array );

						continue;
					}

					if( fieldType == stringT )
						field.SetValue( instance, child.AsString );
					else if( fieldType == intT )
						field.SetValue( instance, child.AsInt );
					else if( fieldType == floatT )
						field.SetValue( instance, child.AsFloat );
					else if( fieldType == boolT )
						field.SetValue( instance, child.AsBool );
					else if( fieldType == doubleT )
						field.SetValue( instance, (double)child.AsFloat );
					else
					{
						// try and get any sub values for the unknown type,
						object n = child.FromObject( fieldType );
						if( n != null )
							field.SetValue( instance, n );
					}
				}

				return (T)instance;
			}

			//----------------------------------------------------------------------------------
			// Properties (Arrays)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Gets all the children of this data element, and returns them as a list
			/// </summary>
			public List<Data> AsList
			{
				get
				{
					if( hierarchyNode == null )
						return new List<Data>();

					List<Data> dataList = new List<Data>( hierarchyNode.Count );

					// get all children from hierarchynode
					for( int i=0; i<hierarchyNode.Count; ++i )
					{
						if( hierarchyNode[i] != null && hierarchyNode[i].Value != null )
							dataList.Add( hierarchyNode[i].Value );
					}

					return dataList;
				}
			}

			/// <summary>
			/// Gets all valid children of this data elements, and returns them as an array
			/// </summary>
			public Data[] AsArray
			{
				get
				{
					if( hierarchyNode == null )
						return new Data[0];

					int count = 0;

					for( int i=0; i<hierarchyNode.Count; ++i )
					{
						if( hierarchyNode[i] != null && hierarchyNode[i].Value != null )
							count++;
					}

					if( count == 0 )
						return new Data[0];

					Data[] arr = new Data[count];
					int index = 0;

					// get all children from hierarchynode
					for( int i=0; i<hierarchyNode.Count; ++i )
					{
						if( hierarchyNode[i] != null && hierarchyNode[i].Value != null )
						{
							arr[index] = hierarchyNode[i].Value;
							index++;
						}
					}

					return arr;
				}
			}

			/// <summary>
			/// The number of children contained within the object/array
			/// </summary>
			public int Count
			{
				get
				{
					if( hierarchyNode == null )
						return 0;

					return hierarchyNode.Count;
				}
			}

			//----------------------------------------------------------------------------------
			// Methods (Child access)
			//----------------------------------------------------------------------------------

			// Access the data with [index] etc return empty objects so can do json["key"][3]["foo"].AsInt and return the
			// default value rather than causing an exception.
			// where checking is needed. use int i=0; if( json["key"][3]["foo"].TryGetInt(out i) )

			/// <summary>
			/// Gets the <see cref="TogglePlay.JSON+Data"/> at the specified index.
			/// If no element is found, a dummy Data instance is returned.
			/// </summary>
			/// <param name="index">Index.</param>
			public Data this[int index]
			{
				get
				{
					return ( hierarchyNode == null || index < 0 || index >= hierarchyNode.Count ) ? 
						new Data(null) :	// Return an empty object so it doesnt cause an exception
						hierarchyNode[index].Value;
				}
			}

			/// <summary>
			/// Gets the <see cref="TogglePlay.JSON+Data"/> at the specified index.
			/// If no element is found, an empty Data instance is returned.
			/// </summary>
			/// <returns>The data element found.</returns>
			/// <param name="index">Index.</param>
			public Data GetElement( int index )
			{
				return this[index];
			}

			/// <summary>
			/// Gets the <see cref="TogglePlay.JSON+Data"/> with the specified key name.
			/// If no element if found, an empty Data instance is returned.
			/// </summary>
			/// <param name="key">Key name of the child to find.</param>
			public Data this[string key]
			{
				get
				{
					if( hierarchyNode == null )
						return null;
					
					Element e = hierarchyNode[key];

					// if it is not found in the children, then create a null child with that key
					if( e == null )
					{
						if( isArray )
							return null;

						if( AddChild( key, null ) )
							return hierarchyNode[key].Value;

						// should not fail to here
						return null;
					}

					return e.Value;
				}
			}

			/// <summary>
			/// Gets the <see cref="TogglePlay.JSON+Data"/> with the specified key name.
			/// If no element if found, an empty Data instance is returned.
			/// </summary>
			/// <returns>The Data of the child found, or empty Data instance if no child found.</returns>
			/// <param name="key">Key name of the child to find.</param>
			public Data GetElement( string key )
			{
				return hierarchyNode == null ? null : hierarchyNode[key].Value;
			}

			/// <summary>
			/// Enumerates through the furthest decendent child element upwards onto
			/// each decendent sibling in turn.
			/// </summary>
			public IEnumerable<Data> DeepChildren
			{
				get
				{
					if( hierarchyNode == null )
						yield break;

					for( int i=0; i<Count; ++i )
					{
						foreach( Data d in hierarchyNode[i].Value.DeepChildrenAndSelf )
							yield return d;
					}
				}
			}

			/// <summary>
			/// Enumerates through the furthest decendent child element upwards onto
			/// each decendent sibling in turn, ending in this data element
			/// </summary>
			public IEnumerable<Data> DeepChildrenAndSelf
			{
				get
				{
					if( hierarchyNode == null )
						yield break;
					
					for( int i=0; i<Count; ++i )
					{
						if( hierarchyNode[i] == null || hierarchyNode[i].Value == null )
							continue;

						foreach( Data d in hierarchyNode[i].Value.DeepChildrenAndSelf )
							yield return d;
					}

					yield return this;
				}
			}

			/// <summary>
			/// Enumerates through all children Data elements
			/// </summary>
			public IEnumerable<Data> Children
			{
				get
				{
					if( hierarchyNode == null )
						yield break;
					for( int i=0; i<Count; ++i )
					{
						if( hierarchyNode[i] == null || hierarchyNode[i].Value == null )
							continue;
						yield return hierarchyNode[i].Value;
					}
				}
			}

			/// <summary>
			/// IEnumerable extention for foreach use.
			/// </summary>
			/// <returns>The enumerator returning each child element.</returns>
			public IEnumerator GetEnumerator()
			{
				if( hierarchyNode == null )
					yield break;
				for( int i=0; i<Count; ++i )
				{
					if( hierarchyNode[i] == null || hierarchyNode[i].Value == null )
						continue;
					yield return hierarchyNode[i].Value;
				}
			}

			//----------------------------------------------------------------------------------
			// Methods (Adding childs)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Create a string of 5 random characters.
			/// Used when adding child data that has no key name to an object
			/// </summary>
			/// <value>The random name.</value>
			string RandomName
			{
				get
				{
					char[] cArr = new char[5];
					for( int i=0; i<6; ++i )
						cArr[i] = (char)UnityEngine.Random.Range( 97, 122 );

					return new string( cArr );
				}
			}

			/// <summary>
			/// Adds a child to this object/array with the provided data
			/// </summary>
			/// <returns><c>true</c>, if child was added, <c>false</c> otherwise.</returns>
			/// <param name="childToAdd">Child Data to add.</param>
			public bool AddChild( Data childToAdd )
			{
				if( hierarchyNode == null && string.IsNullOrEmpty( key ) == false )
					hierarchyNode = new Element( key, this );

				if( childToAdd == null || hierarchyNode == null )
				{
					Debug.LogError( "Failed adding " + childToAdd.key );
					return false;
				}

				if( m_Type == EType.ARRAY )
				{
					// make sure all childrens keys are the correct index
					for( int i=0; i<Count; ++i )
						hierarchyNode[i].Value.key = i.ToString();

					if( childToAdd.hierarchyNode != null )
					{
						return hierarchyNode.Add( childToAdd.hierarchyNode ) != null;
					}
					return hierarchyNode.Add( Count.ToString(), childToAdd ) != null;
//					Element childElement = hierarchyNode.Add( Count.ToString(), childToAdd );
//					if( childElement == null )
//						return false;
//					else
//					{
//						childToAdd.hierarchyNode = childElement;
//						return true;
//					}
				}
				else if( m_Type == EType.OBJECT )
				{
					if( string.IsNullOrEmpty( childToAdd.key ) )
					{
						if( Debug.isDebugBuild )
							Debug.LogWarning( "Adding a child that has no keyname to an object" );

						childToAdd.key = RandomName;
						while( hierarchyNode.ContainsChildKey( childToAdd.key ) )
							childToAdd.key = RandomName;
					}

					if( childToAdd.hierarchyNode != null )
						return hierarchyNode.Add( childToAdd.hierarchyNode ) != null;
					
					return hierarchyNode.Add( childToAdd.key, childToAdd ) != null;
				}
				else
				{
					bool success = false;
					if( childToAdd.hierarchyNode != null )
						success = hierarchyNode.Add( childToAdd.hierarchyNode ) != null;
					else
						success = hierarchyNode.Add( childToAdd.key, childToAdd ) != null;

					if( success )
					{
						// if child added ok make this element an object type
						m_Type = EType.OBJECT;
						return success;
					}
				}
				return false;
			}

			/// <summary>
			/// Adds the child, with the specified string data.
			/// null value will make it a type of NULL
			/// </summary>
			/// <returns><c>true</c>, if child was added, <c>false</c> otherwise.</returns>
			/// <param name="key">Key of the data element.</param>
			/// <param name="value">Value to set to.</param>
			public bool AddChild( string key, string value )
			{
				return AddChild( new Data( key, value ) );
			}

			/// <summary>
			/// Adds the child, with the specified float data
			/// </summary>
			/// <returns><c>true</c>, if child was added, <c>false</c> otherwise.</returns>
			/// <param name="key">Key of the data element.</param>
			/// <param name="value">Value to set to.</param>
			public bool AddChild( string key, float value )
			{
				return AddChild( new Data( key, value ) );
			}

			/// <summary>
			/// Adds the child, with the specified int data
			/// </summary>
			/// <returns><c>true</c>, if child was added, <c>false</c> otherwise.</returns>
			/// <param name="key">Key of the data element.</param>
			/// <param name="value">Value to set to.</param>
			public bool AddChild( string key, int value )
			{
				return AddChild( new Data( key, value ) );
			}

			/// <summary>
			/// Adds the child, with the specified bool data
			/// </summary>
			/// <returns><c>true</c>, if child was added, <c>false</c> otherwise.</returns>
			/// <param name="key">Key of the data element.</param>
			/// <param name="value">Value to set to.</param>
			public bool AddChild( string key, bool value )
			{
				return AddChild( new Data( key, value ) );
			}

			/// <summary>
			/// Adds the empty array child, with the provided key name.
			/// </summary>
			/// <returns><c>true</c>, if child was added successfully, <c>false</c> otherwise.</returns>
			/// <param name="key">Key name of the child.</param>
			public bool AddEmptyArray( string key )
			{
				Data d = new Data();
				d.isArray = true;
				d.key = key;
				return AddChild( d );
			}

			/// <summary>
			/// Adds the empty object child, with the provided key name.
			/// </summary>
			/// <returns><c>true</c>, if the child object was added successfully, <c>false</c> otherwise.</returns>
			/// <param name="key">Key name of the child.</param>
			public bool AddEmptyObject( string key )
			{
				Data d = new Data();
				d.isObject = true;
				d.key = key;
				return AddChild( d );
			}

			/// <summary>
			/// Adds a child with the data of the string value.
			/// Warning  - if adding to an object, it will be given a random key name, only adding to an
			/// array is advised.
			/// </summary>
			/// <returns><c>true</c>, if child was added successfully, <c>false</c> otherwise.</returns>
			/// <param name="value">String value to set as the child data.</param>
			public bool AddChild( string value )
			{
				return AddChild( new Data( null, value ) );
			}

			/// <summary>
			/// Adds a child with the data of the float value.
			/// Warning  - if adding to an object, it will be given a random key name, only adding to an
			/// array is advised.
			/// </summary>
			/// <returns><c>true</c>, if child was added successfully, <c>false</c> otherwise.</returns>
			/// <param name="value">Float value to set as the child data.</param>
			public bool AddChild( float value )
			{
				return AddChild( new Data( null, value ) );
			}

			/// <summary>
			/// Adds a child with the data of the integer value.
			/// Warning  - if adding to an object, it will be given a random key name, only adding to an
			/// array is advised.
			/// </summary>
			/// <returns><c>true</c>, if child was added successfully, <c>false</c> otherwise.</returns>
			/// <param name="value">Int value to set as the child data.</param>
			public bool AddChild( int value )
			{
				return AddChild( new Data( null, value ) );
			}

			/// <summary>
			/// Adds a child with the data of the bool value.
			/// Warning  - if adding to an object, it will be given a random key name, only adding to an
			/// array is advised.
			/// </summary>
			/// <returns><c>true</c>, if child was added successfully, <c>false</c> otherwise.</returns>
			/// <param name="value">Bool value to set as the child data.</param>
			public bool AddChild( bool value )
			{
				return AddChild( new Data( null, value ) );
			}

			/// <summary>
			/// Adds a child of type array.
			/// Warning  - if adding to an object, it will be given a random key name, only adding to an
			/// array is advised.
			/// </summary>
			/// <returns><c>true</c>, if child was added successfully, <c>false</c> otherwise.</returns>
			public bool AddEmptyArray()
			{
				Data d = new Data();
				d.isArray = true;
				return AddChild( d );
			}

			/// <summary>
			/// Adds a child of type object.
			/// Warning  - if adding to an object, it will be given a random key name, only adding to an
			/// array is advised.
			/// </summary>
			/// <returns><c>true</c>, if child was added successfully, <c>false</c> otherwise.</returns>
			public bool AddEmptyObject()
			{
				Data d = new Data();
				d.isObject = true;
				return AddChild( d );
			}

			//----------------------------------------------------------------------------------
			// Methods (General)
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Formats the data to a string
			/// </summary>
			/// <returns>A <see cref="System.String"/> that represents the current <see cref="TogglePlay.JSON+Data"/>.</returns>
			public override string ToString()
			{
				return string.Format( "[JSONData: Key={0}, Type{1}, Value={2}]", key, Type, Value );
			}
		}
	}
}
