﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: The Hierarchy is the root of all elements within the element hierarchy
 * Although Element can act as a root itself, using Hierarchy instead is more readable
 * and gives a defined root object. New root Hierarchies can be created from sub Elements
 * of a different Hierarchy if needed.
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay
{
	public partial class JSON
	{
		public partial class Hierarchy
		{
			//----------------------------------------------------------------------------------
			// Member Variables
			//----------------------------------------------------------------------------------

			/// <summary>
			/// The root element of the hierarchy, This is private and contains no data other than children.
			/// </summary>
			Element root = null;

			//----------------------------------------------------------------------------------
			// Constructors
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Hierarchy"/> class with no elements.
			/// </summary>
			public Hierarchy()
			{
				root = new Element( null, null );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Hierarchy"/> class.
			/// if newElement.Key is empty then all children will be added to the first hierarchy level,
			/// else the new element will be added to the first hierarchy level.
			/// </summary>
			/// <param name="root">Root.</param>
			public Hierarchy( Element newElement )
			{
				root = new Element( null, null );

				if( object.Equals( newElement.Key, null ) )
				{
					for( int i=0; i<newElement.Count; ++i )
						Add( newElement[i] );
				}
				else
					Add( newElement );
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+Hierarchy"/> class with an array of elements.
			/// </summary>
			/// <param name="newElements">New elements.</param>
			public Hierarchy( Element[] newElements )
			{
				root = new Element( null, null );

				for( int i=0; i<newElements.Length; ++i )
					Add( newElements[i] );
			}

			//----------------------------------------------------------------------------------
			// Utility Properties
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Gets the number of direct children stored
			/// </summary>
			/// <value>The number of child elements.</value>
			public int Count
			{
				get{ return root.Count; }
			}

			//----------------------------------------------------------------------------------
			// Access Operators and Methods
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Gets Element at the index
			/// If a json array, this will be the value at the array index.
			/// If a json object, this will be the element added order index.
			/// </summary>
			/// <param name="index">Index.</param>
			public Element this[int index]
			{
				get{ return root[index]; }
			}

			/// <summary>
			/// Gets the element at the specified index.
			/// Does not check for within range.
			/// </summary>
			/// <returns>The element stored at child index.</returns>
			/// <param name="index">Index of the child element.</param>
			public Element GetElement( int index )
			{
				return root[index];
			}

			/// <summary>
			/// Gets the value at specified index.
			/// Does not check for within range.
			/// </summary>
			/// <returns>The value stored at child index.</returns>
			/// <param name="index">Index of the child element.</param>
			public JSON.Data GetValue( int index )
			{
				return root[index].Value;
			}

			/// <summary>
			/// Gets the element with the specified key name.
			/// A dummy object will be created if not found.
			/// </summary>
			/// <param name="key">Key name.</param>
			public Element this[string key]
			{
				get{ return root[key]; }
			}

			/// <summary>
			/// Gets the element with the specified key name.
			/// A dummy object will be created if not found.
			/// </summary>
			/// <returns>The element found.</returns>
			/// <param name="key">Key name.</param>
			public Element GetElement( string key )
			{
				return root[key];
			}

			/// <summary>
			/// Gets the value with the specified key.
			/// Does not check for within range.
			/// </summary>
			/// <returns>The value of the child with key.</returns>
			/// <param name="key">Key to search for.</param>
			public JSON.Data GetValue( string key )
			{
				return root[key].Value;
			}

			/// <summary>
			/// Gets the element through the hierarchy.
			/// Each string within the array keyPath will look for a child
			/// with that name.
			/// </summary>
			/// <param name="keyPath">Key path through children.</param>
			public Element this[string[] keyPath]
			{
				get{ return root.GetElement( keyPath ); }
			}

			/// <summary>
			/// Gets the element through the hierarchy.
			/// Each string within the array keyPath will look for a child
			/// with that name.
			/// </summary>
			/// <returns>The Element found.</returns>
			/// <param name="keyPath">Key path through children.</param>
			public Element GetElement( string[] keyPath )
			{
				Element e = root;
				if( e == null || keyPath == null || keyPath.Length == 0 )
					return null;

				for( int i=0; i<keyPath.Length; ++i )
				{
					e = e[keyPath[i]];
					if( e == null )
						break;
				}

				return e;
			}

			//----------------------------------------------------------------------------------
			// Insertion
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Adds a new child element.
			/// </summary>
			/// <param name="key">Key.</param>
			/// <param name="value">Value.</param>
			public Element Add( string key, JSON.Data value )
			{
				return root.Add( key, value );
			}

			/// <summary>
			/// Adds a new child element.
			/// </summary>
			/// <param name="element">Element to add.</param>
			public Element Add( Element element )
			{
				return root.Add( element );
			}

			/// <summary>
			/// Inserts a new element at a specific index.
			/// </summary>
			/// <param name="index">Index to insert into.</param>
			/// <param name="key">Key.</param>
			/// <param name="value">Value.</param>
			public Element Insert( int index, string key, JSON.Data value )
			{
				return root.Insert( index, key, value );
			}

			//----------------------------------------------------------------------------------
			// Other
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Returns a string representing each of the child elements within the hierarchy.
			/// </summary>
			public override string ToString()
			{
				string str = "";

				for( int i=0; i<root.Count; ++i )
				{
					if( i != 0 )
						str += "\n";
					str += root[i].ToString();
				}

				return str;
			}

			public string ToJSON()
			{
				// TODO need some way of seeing if root is an array or object
				return "P";
			}

			/// <summary>
			/// Clears the hierarchy of all children.
			/// </summary>
			public void Clear()
			{
				root = null;
			}
		}
	}
}
