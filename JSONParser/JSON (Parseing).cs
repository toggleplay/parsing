﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: This contains the parts of the JSON class that perform the parsing
 * of content to and from strings
 * 
 * Tasks:
 * Parse Hierarchy to JSON string
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay
{
	/// <summary>
	/// This part of the JSON class is to focus on the Parsing of data
	/// </summary>
	public partial class JSON : IEnumerable
	{
		//----------------------------------------------------------------------------------
		// Member Declarations
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Indicates if an error has occured when trying to parse a json document.
		/// </summary>
		public enum EParseResult
		{
			SUCCESS = 0,
			NEW_OBJECT_FAILURE,
			NEW_TOKEN_FAILURE,
			TOO_MANY_CLOSING_BRACKETS,
			ENDED_IN_QUOTES,
			HIERARCHY_ERROR,
			OTHER_ERROR
		}

		/// <summary>
		/// Indicates what type of data is currently being parsed
		/// </summary>
		public enum ParsingType
		{
			OBJECT = 0,
			ARRAY,
			DATA
		}

		/// <summary>
		/// Class to store temporary data used when parsing. Saving sending unnessecary data
		/// </summary>
		private class ParsingData
		{
			/// <summary>
			/// An instance of the Data object, used to store information about data within the json structure
			/// </summary>
			public Data activeDataElement = new Data();

			/// <summary>
			/// The active json object within the hierarchy (object or array)
			/// </summary>
			public Element activeObject = null;

			/// <summary>
			/// Where to start copying data from the json string
			/// </summary>
			public int charStart = -1;

			/// <summary>
			/// How many characters to copy from the json string
			/// </summary>
			public int charCount = 0;

			/// <summary>
			/// Start with an array length of 32 and double it whenever nessecary
			/// </summary>
			public char[] tokenArray = new char[32];

			/// <summary>
			/// how many characters in the token array are currently being used
			/// </summary>
			public int tokenLength = 0;

			/// <summary>
			/// The type of data to parse as
			/// </summary>
			public ParsingType type = ParsingType.DATA;

			/// <summary>
			/// Double characters to the char array if it isnt big enough
			/// </summary>
			public void CheckLength( )
			{
				while( tokenArray.Length < tokenLength + charCount )
				{
					char[] newArr = new char[ tokenArray.Length * 2 ];
					tokenArray.CopyTo( newArr, 0 );
					tokenArray = newArr;
				}
			}
			
			/// <summary>
			/// Add another 8 characters to the char array if it isnt big enough
			/// </summary>
			/// <param name="charCount">The amount of character space needed.</param>
			public void CheckLength( int charCount )
			{
				while( tokenArray.Length < tokenLength + charCount )
				{
					char[] newArr = new char[ tokenArray.Length + 8 ];
					tokenArray.CopyTo( newArr, 0 );
					tokenArray = newArr;
				}
			}

			/// <summary>
			/// Copies the token data from the json string to the token char array
			/// </summary>
			/// <param name="jsonString">Json string to copy from.</param>
			public void CopyTokenData( string jsonString )
			{
				if( charStart != -1 )
				{
					CheckLength();
					jsonString.CopyTo( charStart, tokenArray, tokenLength, charCount );
					tokenLength += charCount;
					charStart = -1;
				}
			}
		}

		//----------------------------------------------------------------------------------
		// Static
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Set to true to try and cast the input value to a type on Init, false otherwise.
		/// </summary>
		static bool checkTypeOnSet = false;

		/// <summary>
		/// Set to true and white space will be removed, or false and it will be read and set as a string
		/// e.g. "name" : Fred of Arandel, will become " Fred of Arandel" when false, "FredofArandel" when true
		/// "idNumber" : 23 124, will become the string " 23 124" when false, and int 23124 when true
		/// </summary>
		static bool ignoreOutOfQuotesWhitespace = false;

		//----------------------------------------------------------------------------------

		/// <summary>
		/// Parse the specified jsonString and return a JSON class instance containing the data
		/// </summary>
		/// <param name="jsonString">Json string to parse.</param>
		public static JSON Parse( string jsonString )
		{
			JSON j = new JSON();
			EParseResult result = j.ParseString(jsonString);

			// check what the result was and log any errors
			switch( result )
			{
			case EParseResult.SUCCESS:
				return j;

			case EParseResult.TOO_MANY_CLOSING_BRACKETS:
				Debug.LogError( "Too many closing brackets within the json text" );
				return null;

			case EParseResult.ENDED_IN_QUOTES:
				Debug.LogError( "Json text ending while still encapsulated within quotation marks" );
				return null;

			case EParseResult.NEW_OBJECT_FAILURE:
				Debug.LogError( "Failed to create new json object" );
				return null;

			case EParseResult.NEW_TOKEN_FAILURE:
				Debug.LogError( "Failed to create new json element" );
				return null;

			case EParseResult.HIERARCHY_ERROR:
				Debug.LogError( "Parsing failed due to data structure" );
				return null;

			default:
				Debug.LogError( "Error parsing json text" );
				return null;
			}
		}

		//----------------------------------------------------------------------------------

		//----------------------------------------------------------------------------------
		// String Parsing to Json Hierarchy
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Parses the string and store the data within an hierarchy structure
		/// </summary>
		/// <returns>Enum indicating if the parse was successful or if an error occured.</returns>
		/// <param name="jsonString">Json string to parse.</param>
		public EParseResult ParseString( string jsonString )
		{
			int charIndex = 0;
			bool isQuotationMarks = false;

			ParsingData temp = new ParsingData();

			//----------------------------------------------------------------------------------

			while( charIndex < jsonString.Length )
			{
				if( isQuotationMarks )
				{
					switch( jsonString[charIndex] )
					{
					case '"':
						temp.CopyTokenData( jsonString );
						isQuotationMarks = !isQuotationMarks;
						break;
						
					case '\\':
						temp.CopyTokenData( jsonString );
						++charIndex;

						temp.CheckLength( 1 );
						switch( jsonString[charIndex] )
						{
						case 't':
							temp.tokenArray[temp.tokenLength] = '\t';
							temp.tokenLength ++;
							break;
						case 'r':
							temp.tokenArray[temp.tokenLength] = '\r';
							temp.tokenLength ++;
							break;
						case 'n':
							temp.tokenArray[temp.tokenLength] = '\n';
							temp.tokenLength ++;
							break;
						case 'b':
							temp.tokenArray[temp.tokenLength] = '\b';
							temp.tokenLength ++;
							break;
						case 'f':
							temp.tokenArray[temp.tokenLength] = '\f';
							temp.tokenLength ++;
							break;
						case 'u':
						{
							string s = jsonString.Substring( charIndex + 1, 4 );
							temp.tokenArray[temp.tokenLength] = (char)int.Parse( s, System.Globalization.NumberStyles.AllowHexSpecifier );
							temp.tokenLength ++;
							charIndex += 4;
							break;
						}
						default:
							temp.tokenArray[temp.tokenLength] = jsonString[charIndex];
							temp.tokenLength ++;
							break;
						}
						break;

					default:
						if( temp.charStart == -1 )
						{
							temp.charStart = charIndex;
							temp.charCount = 1;
						}
						else
						{
							++temp.charCount;
						}
						break;
					}
				}
				else
				{
					switch( jsonString[charIndex] )
					{
					case '"':
						// Entering quote marks, no need to do anything with tokens
						isQuotationMarks = !isQuotationMarks;
						break;
						
					case ':':
						// Concat the previous text to be forgiving to mission quotes
						temp.CopyTokenData( jsonString );

						temp.activeDataElement.key = new string( temp.tokenArray, 0, temp.tokenLength );
						temp.tokenLength = 0;
						break;
						
						//----------------------------------------------------------------------------------
						// End of an element
					case ',':
						// could be an array of numbers etc so need to concat
						temp.CopyTokenData( jsonString );
						temp.type = ParsingType.DATA;
						AddToken( temp );
						break;
						
						//----------------------------------------------------------------------------------
						// Open new object or array
					case '[':
					case '{':
						if( temp.activeObject == null )
						{
							Data rootData = new Data();
							if( jsonString[charIndex] == '[' )
								rootData.isArray = true;
							else
								rootData.isObject = true;

							rootData.key = "";
							Element newNode = new Element( "", rootData );
							rootData.hierarchyNode = newNode;
							temp.activeObject = rootData.hierarchyNode;

							if( temp.activeObject == null )
							{
								Debug.Log( "Root fail" );
								return EParseResult.NEW_OBJECT_FAILURE;
							}
							break;
						}

						if( jsonString[charIndex] == '[' )
							temp.type = ParsingType.ARRAY;
						else
							temp.type = ParsingType.OBJECT;

						// create new data for the object storage. Adding it to the current active Object
						Data d = AddToken( temp );
						if( d == null )
						{
							Debug.Log( "Token");
							return EParseResult.NEW_TOKEN_FAILURE;
						}

						// set the new active element to the newly created node
						temp.activeObject = d.hierarchyNode;
						if( temp.activeObject == null )
						{
							return EParseResult.HIERARCHY_ERROR;
						}

						// set the new active element to the newly created node
						temp.activeObject = d.hierarchyNode;
						break;
						
						//----------------------------------------------------------------------------------
						// End of an object or array
					case '}':
					case ']':
						temp.CopyTokenData( jsonString );

						// if it has no activeObject then it has either already closed too many times,
						// or an object has not yet been opened.
						if( temp.activeObject == null )
						{
							return EParseResult.TOO_MANY_CLOSING_BRACKETS;
						}
						else
						{
							temp.type = ParsingType.DATA;
							AddToken( temp );
							
							if( temp.activeObject.Parent != null )
								temp.activeObject = temp.activeObject.Parent;
						}
						break;
						
						//----------------------------------------------------------------------------------
						// Other characters
						
					case '\\':
						++charIndex;
						break;

					case ' ':
					case '\t':
					case '\r':
					case '\n':
						// concat as hit white space when not in quotes
						if( ! ignoreOutOfQuotesWhitespace )
						{
							if( temp.charStart != -1 )
							{
								++temp.charCount;
								temp.CheckLength();
								jsonString.CopyTo( temp.charStart, temp.tokenArray, temp.tokenLength, temp.charCount );
								temp.tokenLength += temp.charCount;
								temp.charStart = -1;
							}
							else
								++temp.charCount;
						}
						else
						{
							temp.CopyTokenData( jsonString );
						}
						break;

					default:
						if (temp.charStart == -1)
						{
							temp.charStart = charIndex;
							temp.charCount = 1;
						}
						else
						{
							++temp.charCount;
						}
						break;
					}
				}

				++charIndex;
			}

			//----------------------------------------------------------------------------------
			// Finalise Parsing

			if( isQuotationMarks )
			{
				return EParseResult.ENDED_IN_QUOTES;
			}

			if( temp.activeObject == null || temp.activeObject.Parent != null )
			{
				return EParseResult.OTHER_ERROR;
			}

			// finalise the parsing by creating an Hierarchy container to interact with
			m_Hierarchy = new Hierarchy( temp.activeObject.ToArray() );
			return EParseResult.SUCCESS;
		}

		//----------------------------------------------------------------------------------

		/// <summary>
		/// Adds the current read token data into the json hierarchy structure
		/// </summary>
		/// <returns>The token Data.</returns>
		/// <param name="temp">Temp stored data if the read json.</param>
		Data AddToken( ParsingData temp )
		{
			// cannot add token if a valid object hasnt yet been opened
			if( temp.activeObject == null )
			{
				if( Debug.isDebugBuild )
					Debug.LogError( "active object null" );

				return null;
			}

			// See if it is an array
			if( string.IsNullOrEmpty(temp.activeDataElement.key) )
			{
				if( temp.activeObject.Value.isArray )
				{
					// check if the text is white noise
					if( temp.type == ParsingType.DATA && temp.tokenLength == 0 )
						return null;

					// it is an array and has no name
					temp.activeDataElement.key = temp.activeObject.Count.ToString();
				}
				else
				{
					// cannot have a non name element that is not in an array
					temp.activeDataElement.key = "";
					return null;
				}
			}

			if( temp.type == ParsingType.ARRAY )
				temp.activeDataElement.isArray = true;
			else if( temp.type == ParsingType.OBJECT )
				temp.activeDataElement.isObject = true;

			// add a new element to the activeObject
			Element activeHeirarchyChild = temp.activeObject.Add( temp.activeDataElement.key, temp.activeDataElement );

			// If it failed see why
			if( activeHeirarchyChild == null )
			{
				if( Debug.isDebugBuild )
				{
					if( temp.activeObject.ContainsChildKey( temp.activeDataElement.key ) )
						Debug.LogError( "Failed adding new json node, Node name \"" + temp.activeDataElement.key + "\" already exists in " + temp.activeObject.Key );
					else
						Debug.LogError( "Failed to add new hierarchy node for JSON data." );
				}
				temp.activeDataElement.key = "";
			}
			else
			{
				temp.activeDataElement.Init( temp.tokenArray, temp.tokenLength );
				temp.tokenLength = 0;
			}

			temp.activeDataElement = new Data();
			return activeHeirarchyChild.Value;
		}

	}
}
