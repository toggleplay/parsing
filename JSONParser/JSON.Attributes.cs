﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Contains any PropertyAttributes that are used within the json parser
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System;

namespace TogglePlay
{
	
	public partial class JSON : System.Collections.IEnumerable
	{
		/// <summary>
		/// Property Attribute used to tell the Reflection parser that a field name is
		/// to use a different key name when looking up the json text than its own field name
		/// </summary>
		[AttributeUsage( AttributeTargets.Field, AllowMultiple = true )]
		public class KeyName : UnityEngine.PropertyAttribute
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.JSON+KeyName"/> class.
			/// </summary>
			/// <param name="keyName">Key name to look up in the json text.</param>
			public KeyName( string keyName )
			{
				this.m_KeyName = keyName;
			}

			/// <summary>
			/// Key name to use to look up data within the json text
			/// </summary>
			protected string m_KeyName;

			/// <summary>
			/// Gets the name of the key to use to look up data within the json text
			/// </summary>
			/// <value>The key name for json string lookup.</value>
			public string keyName 
			{
				get{ return this.m_KeyName; }
			}
		}
	}

}
